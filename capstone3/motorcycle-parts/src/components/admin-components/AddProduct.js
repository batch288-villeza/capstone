import {Form, Button, Offcanvas, InputGroup, Card} from 'react-bootstrap';
import {useState} from 'react';

import Swal2 from 'sweetalert2';
import avatar from '../image/placeholder.png'

export default function AddProduct() {

	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState('');
	const [image, setImage] = useState('');
	// const [isActiveChecked, setIsActiveChecked] = useState(true);

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	// state for image image
	const [postImage, setPostImage] = useState( { myFile : ""})

	// function for image upload
	const handleFileUpload = async (e) => {
		setImage(e.target.value)

		const file = e.target.files[0];
		const base64 = await convertToBase64(file);
		// console.log(base64)
		setPostImage({ ...postImage, myFile : base64 })
	}

	// convert file to base64 format
	function convertToBase64(file){
	  return new Promise((resolve, reject) => {
	    const fileReader = new FileReader();
	    fileReader.readAsDataURL(file);
	    fileReader.onload = () => {
	      resolve(fileReader.result)
	    };
	    fileReader.onerror = (error) => {
	      reject(error)
	    }
	  })
	}

	// add products
	const addNewProduct = (e) =>{
		e.preventDefault()

		const convertPrice = Number(price)
		const convertQty = Number(quantity)

		if(localStorage.getItem('token')){
			fetch(
				`${process.env.REACT_APP_API_URL}/products/addProduct`,
				{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						 productName: productName
						,description: description
						,price: convertPrice
						,quantity: convertQty
						// ,productImage: postImage.myFile
					})
				}
			)
			.then(response => response.json())
			.then(data => {
				// console.log(data)
				if(data){
					Swal2.fire({
						title: `${productName} is successfully added!`,
						icon: 'success'
					})

					setProductName('')
					setDescription('')
					setPrice('')
					setQuantity('')
					setImage('')
					setPostImage({ myFile : ""})

				}else{
					// console.log(data)
					Swal2.fire({
						title: 'Something went wrong. Please try again!',
						icon: 'error'
					})
				}
			})
		}
	}

	return(
		<>
			<Button variant="success" onClick={handleShow}><i className="bi bi-file-earmark-plus"></i> Add Product</Button>

			<Offcanvas show={show} onHide={handleClose} backdrop='true' scroll='true'>

				<Offcanvas.Header closeButton></Offcanvas.Header>

				<Offcanvas.Body>
					<Card>
						<div className="bg-dark text-light text-center mb-3 py-2 rounded-top fs-3">
							<span>Create New Product</span>
						</div>

						<Card.Img variant="top" src={postImage.myFile || avatar} alt="Product Image"/>
						
						<Card.Body>
								
								<div className="mx-3 pb-3">
									<Form onSubmit={event => addNewProduct(event)}>
										<Form.Group className="mb-3" controlId="productName">
											<Form.Label>Name</Form.Label>
											<Form.Control
												type="text"
												placeholder="Product Name"
												value={productName}
												onChange={event => setProductName(event.target.value)}
												required
											/>
										</Form.Group>

										<Form.Group className="mb-3" controlId="description">
											<Form.Label>Description</Form.Label>
											<Form.Control
												as="textarea"
												rows={4}
												placeholder="Product Description"
												value={description}
												onChange={event => setDescription(event.target.value)}
												required
											/>
										</Form.Group>

										<Form.Group className="mb-3" controlId="description">
											<Form.Label>Price</Form.Label>
											<InputGroup className="mb-3">
												<InputGroup.Text>₱</InputGroup.Text>
												<Form.Control
													aria-label="Amount (to the nearest philippine peso)"
													placeholder="0.00"
													value={price}
													onChange={event => setPrice(event.target.value)}
													onKeyPress={
														(event) => {
			                                            	if (!/^[0-9]*\.?[0-9]*$/.test(event.key)) {
			                                              		event.preventDefault();
			                                            	}
			                                          	}
			                                        }
													required
		                                        />
											</InputGroup>
										</Form.Group>

										<Form.Group className="mb-3" controlId="quantity">
											<Form.Label>Quantity</Form.Label>
											<Form.Control
												aria-label="Quantity of product"
												placeholder="0.00"
												value={quantity}
												onChange={event => setQuantity(event.target.value)}
												onKeyPress={
													(event) => {
		                                            	if (!/^[0-9]*\.?[0-9]*$/.test(event.key)) {
		                                              		event.preventDefault();
		                                            	}
		                                          	}
		                                        }
												required
		                                    />
										</Form.Group>

										<Form.Group controlId="formFile" className="mb-3">
											<Form.Label>Product Image</Form.Label>
											<Form.Control
												type="file"
												name="myFile"
												value={image}
												accept='.jpeg, .png, .jpg'
									          	onChange={(e) => handleFileUpload(e)}
												// required
											/>
										</Form.Group>

										<div className="d-grid my-3">
											<Button
												variant="dark"
												type="submit"
											>Add Product</Button>
										</div>
									</Form>
								</div>
						</Card.Body>	
					</Card>
				</Offcanvas.Body>
			</Offcanvas>
		</>
	)
}

