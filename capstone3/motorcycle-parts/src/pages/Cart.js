import { Container, Row, Col } from "react-bootstrap"

import CartCatalog from '../components/cart-components/CartCatalog.js'
export default function Cart(){

	return(
		<div className="margin-from-navbar content-bg">
			<Container>
				<Row className="bg-white">
					<Col className="col-12 col-lg-9 order-2 order-lg-1">
						<div className="scroll-y-container">
							<CartCatalog/>
						</div>
					</Col>
					<Col className="col-12 col-lg-3 order-1 order-lg-2">
						<div className="">
							<p>Address</p>
						</div>
					</Col>
				</Row>
			</Container>
		</div>
	)
}